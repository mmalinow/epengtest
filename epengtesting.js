/*
 *  _ Test der Vodafone ePeng-Schnittstelle _
 *  Die Schnittstelle wird daraufhin getestet, ob sie mindestens ein Grerät mit den wichtigsten Daten enthält 
 *  und wie viele Geräte verfügbar sind. ePeng lieferte immer eine Antwort, allerdings war der Wurzelknoten oft leer.
 *
 *  Dateiname:epengtesting.js
 *  Autor: Martin Malinowski (martin.malinowski@sevenval.com)
 *  Version: 0.001 Alpha
 *  Datum: 23.09.2013 
 */


var casper = require("casper").create({
    logLevel: "debug"
});

casper.start("http://media.vodafone.de/enterprise/media/exportdata.xml", function() {
  this.test.assertExists({type:'xpath',path: 'mobiles'},"Rootknoten exisitiert");
  this.test.assertExists({type:'xpath',path: 'mobiles/mobile'},"mindestens ein Geraet exisitiert");
  this.test.assertExists({type:'xpath',path: 'mobiles/mobile/name'},"Es gibt einen Geraetenamen");
  this.test.assertExists({type:'xpath',path: 'mobiles/mobile/priceNetTariff'},"Es gibt einen Nettopreis");
  this.test.assertExists({type:'xpath',path: 'mobiles/mobile/priceGrossTariff'},"Es gibt einen Bruttopreis");
  this.test.assertExists({type:'xpath',path: 'mobiles/mobile/imagepaths/small'},"Es gibt ein Bild");
  this.test.assertEval(function() {
    return __utils__.findAll({type:'xpath',path: 'mobiles/mobile'}).length >= 10;
  }, "es gibt mehr als 10 Geraete");
  this.test.assertEval(function() {
    return __utils__.findAll({type:'xpath',path: 'mobiles/mobile'}).length >= 100;
  }, "es gibt mehr als 100 Geraete");
});

casper.run(function() {
    this.test.renderResults(true);
});
